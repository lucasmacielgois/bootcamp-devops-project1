resource "aws_instance" "amb-prod"{
  vpc_security_group_ids = ["allow_ssh", "allow_http", "allow_egress"]
  ami           = "ami-01b799c439fd5516a"
  instance_type = "t2.micro"
  key_name = "devops-bootcamp"

  user_data = file("script.sh")




  tags = {
    Name = "amb-prod"
  }
}