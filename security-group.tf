resource "aws_security_group" "allow_ssh" {
  name_prefix = "allow_ssh"
  description = "Permitir acesso remoto via porta 22(ssh)"
  
  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
}

resource "aws_security_group" "allow_http" {
  name_prefix = "allow_http"
  description = "Permitir acesso remoto via porta 80(http)"

  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
}

resource "aws_security_group" "allow_egress" {
  name_prefix = "allow_egress"
  description = "Permitir acesso remoto via porta 80(http)"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}